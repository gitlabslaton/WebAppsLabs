/*
 * task.spec.js
 *
 * Test file for your task class
 */
var expect, Task;

expect = require('./chai.js').expect;

Task = require('./task.js');

// ADD YOUR TESTS HERE



describe('Your code for tasks', function() {
  it('defines a variable Task.new', function() {
    expect(function() { Task.new; }).to.not.throw(Error)

  });

   // Add more "it" sections below
  it('actually defines a function new', function() {
    expect(Task.new).to.be.a('function');
  });

  it('defines a variable Task.fromString', function() {
    expect(function() { Task.fromString; }).to.not.throw(Error)

  });

   // Add more "it" sections below
  it('actually defines a function fromString', function() {
    expect(Task.fromString).to.be.a('function');
  });

  it('defines a variable Task.fromObject', function() {
    expect(function() { Task.fromObject; }).to.not.throw(Error)

  });

   // Add more "it" sections below
  it('actually defines a function new', function() {
    expect(Task.fromObject).to.be.a('function');
  });

});


describe('Your makeNewTask function', function() {
  var task, task2;
  task = Task.new();
  task2 = Task.new();
  
  it('returns an object', function() {
    expect(task).to.be.a('object');
  });

  it('returns an object with keys id, title and completedTime', function() {
    ['id', 'title', 'completedTime'].forEach(function(key) {
      expect(task[key]).to.not.equal(undefined);
    });
  });

  it('returns object without enumerable tags', function() {
    var keys = Object.keys(task);
    keys.forEach(function(key) {
      expect(key).to.not.equal('tags');
    });

  });

  it('returns object with tags key initialized to empty array', function () {
    expect(task['tags'].length).to.equal(0);
  });

  it('has id of 1 for first task', function() {
    expect(task.id).to.equal(1);
  });

  it('has id of 2 for second task', function() {
    expect(task2.id).to.equal(2);
  });

  it('title initially is empty', function() {
    expect(task.title).to.equal('');
  });

  it('completed time initially is null', function() {
    expect(task.completedTime).to.equal(null);
  });


});

describe('Your makeTaskFromObject function', function() {
  var task, oTask, oTask2;

  task = Task.new();
  oTask = Task.fromObject(task);
  
  it('returns an object', function() {
    expect(oTask).to.be.a('object');
  });

  it('from empty task: has emptry string title', function() {
    expect(oTask.title).to.equal('');
  });

  it('from empty task: has null completed time', function() {
    expect(oTask.completedTime).to.equal(null);
  });

  it('from empty task: has no tags', function() {
    expect(oTask.tags.length).to.equal(0);
  });

  it('from empty task: has different id', function() {
    expect(oTask.id).to.not.equal(task.id);
  });

  task.setTitle('someTitle');
  task.addTag('yesTag');

  oTask2 = Task.fromObject(task);

  it('from  task: has same title', function() {
    expect(oTask2.title).to.equal(task.title);
  });

  it('from task: has null completed time', function() {
    expect(oTask2.completedTime).to.equal(null);
  });

  it('from empty task: has same tags', function() {
    expect(oTask2.hasTag('yesTag')).to.equal(true);
    expect(oTask2.tags.length).to.equal(1);
  });

  it('from task: has different id', function() {
    expect(oTask2.id).to.not.equal(task.id);
    expect(oTask2.id).to.not.equal(oTask.id);
  });

});


describe('Your makeTaskFromString function', function() {
  var testStr, strTask, strTask2, title, tags;
  
  testStr = '';

  strTask = Task.fromString(testStr);

  it('returns an object', function() {
    expect(strTask).to.be.a('object');
  });

  it('from empty String: has emptry string title', function() {
    expect(strTask.title).to.equal('');
  });

  it('from empty string: has null completed time', function() {
    expect(strTask.completedTime).to.equal(null);
  });

  it('from empty string: has no tags', function() {
    expect(strTask.tags.length).to.equal(0);
  });

  title = 'hi there!';
  tags = '#mytag #theirtag'
  testStr = title + tags;
  strTask2 = Task.fromString(testStr);

  it('from string: has correct title', function() {
    expect(strTask2.title).to.equal(title);
  });

  it('from string: has null completed time', function() {
    expect(strTask2.completedTime).to.equal(null);
  });

  it('from string: has correct tags', function() {
    expect(strTask2.hasTag('mytag')).to.equal(true);
  });

  it('from string: has correct tags', function() {
    expect(strTask2.hasTag('theirtag')).to.equal(true);
    expect(strTask2.tags.length).to.equal(2);
  });

  it('from string: has different id', function() {
    expect(strTask2.id).to.not.equal(strTask.id);
  });

});


describe('Your prototype: ', function() {
  var task = Task.new();

  it('returns instance methods', function() {
    var methods;
  
    methods = ['setTitle', 'isCompleted', 'toggleCompleted', 
      'hasTag', 'addTag', 'removeTag', 'toggleTag', 
      'addTags', 'removeTags', 'toggleTags', 'clone'];

    methods.forEach(function(m){
      expect(task[m]).to.be.a('function');
    })
  });

  it('correctly sets title', function() {
    task.setTitle('hello');
    expect(task.title).to.equal('hello');
  });

  it('isCompleted returns false after intialization', function() {
    expect(task.isCompleted()).to.equal(false);
  });

  it('toggleCompleted returns task object', function() {
    var temp = task.toggleCompleted();
    expect(task).to.equal(temp);
  });

  // it('toggleCompleted sets completedTime to current time', function() {
  //   expect(task.completedTime).to.equal();
  // });

  it('isCompleted returns true after toggleCompleted', function() {
    expect(task.isCompleted()).to.equal(true);
  });

  it('second toggleCompleted toggles it back to null', function() {
    task.toggleCompleted();
    expect(task.isCompleted()).to.equal(false);
  });

  it('hasTag returns false if no tag', function() {
    expect(task.hasTag('')).to.equal(false);
  });

  it('addTag adds a tag and hasTag(tag) returns true for it', function() {
    task.addTag('newTag');
    expect(task.hasTag('newTag')).to.equal(true);
    expect(task.tags.length).to.equal(1);
  });

  it('addTag does not add the same tag twice', function() {
    task.addTag('newTag');
    expect(task.hasTag('newTag')).to.equal(true);
    expect(task.tags.length).to.equal(1);
  });

   it('removeTag returns task object', function() {
    var temp = task.removeTag('oldTag');
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(task.tags.length).to.equal(1);
    expect(task).to.equal(temp);
  });

  it('removeTag removes tag', function() {
    task.removeTag('newTag');
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(task.tags.length).to.equal(0);
  });

  it('toggleTag returns task object', function() {
    var temp = task.toggleTag('oldTag');
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(task.tags.length).to.equal(1);
    expect(task).to.equal(temp);
  });


  it('toggleTag correctly adds tag', function() {
    var temp = task.toggleTag('someTag');
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(task.tags.length).to.equal(2);
    expect(task.hasTag('someTag')).to.equal(true);
  });

  it('toggleTag correctly removes tag', function() {
    var temp = task.toggleTag('someTag');
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(task.tags.length).to.equal(1);
    expect(task.hasTag('someTag')).to.equal(false);
  });

  it('addTags returns task object', function() {
    var temp = task.addTags(['tag1', 'tag2']);
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(temp).to.equal(task);
  });

  it('addTags correctly added tags', function() {
    var temp = task.addTags(['tag1', 'tag2']);
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(task.hasTag('tag1')).to.equal(true);
    expect(task.hasTag('tag2')).to.equal(true);
    expect(task.tags.length).to.equal(3);
    expect(temp).to.equal(task);
  });

  it('addTags with empty array does not add any tags', function() {
    var temp = task.addTags([]);
    expect(task.tags.length).to.equal(3);
  });


   it('removeTags returns task object', function() {
    var temp = task.removeTags(['tag1', 'tag2']);
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(temp).to.equal(task);
  });

  it('removeTags correctly added tags', function() {
    var temp = task.removeTags(['tag1', 'tag2']);
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(task.hasTag('tag1')).to.equal(false);
    expect(task.hasTag('tag2')).to.equal(false);
    expect(task.tags.length).to.equal(1);
    expect(temp).to.equal(task);
  });

  it('removeTags with empty array does not add any tags', function() {
    var temp = task.removeTags([]);
    expect(task.tags.length).to.equal(1);
  });

  it('toggleTags returns task object', function() {
    var temp = task.removeTags(['tag1', 'tag2']);
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(temp).to.equal(task);
  });

  it('removeTags correctly added tags', function() {
    var temp = task.toggleTags(['tag1', 'tag2']);
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(task.hasTag('tag1')).to.equal(true);
    expect(task.hasTag('tag2')).to.equal(true);
    expect(task.tags.length).to.equal(3);
    expect(temp).to.equal(task);
  });

  it('removeTags correctly removed tags', function() {
    var temp = task.toggleTags(['tag1', 'tag2']);
    //expect(task.hasTag('oldTag')).to.equal(false);
    expect(task.hasTag('tag1')).to.equal(false);
    expect(task.hasTag('tag2')).to.equal(false);
    expect(task.tags.length).to.equal(1);
    expect(temp).to.equal(task);
  });

  it('toggleTags with empty array does not add any tags', function() {
    var temp = task.toggleTags([]);
    expect(task.tags.length).to.equal(1);
  });

  it('clone returns object with different ID', function() {
    var temp = task.clone();
    expect(temp.id).to.not.equal(task.id);
  });

  it('clone returns object with same title', function() {
    var temp = task.clone();
    expect(temp.id).to.not.equal(task.id);
    expect(temp.title).to.equal(task.title);
    expect(temp.completedTime).to.equal(task.completedTime);
  });

  it('clone returns object with same completion status', function() {
    var temp = task.clone();
    expect(temp.id).to.not.equal(task.id);
    expect(temp.completedTime).to.equal(task.completedTime);
  });

  it('clone returns object with same tag list', function() {
    task.addTag('anotherTag');
    var temp = task.clone();
    expect(temp.id).to.not.equal(task.id);
    expect(temp.hasTag('oldTag')).to.equal(true);
    expect(temp.hasTag('anotherTag')).to.equal(true);
    expect(temp.tags.length).to.equal(2);
  });

  it('clone returns object with same tag list if empty', function() {
    task.removeTags(['oldTag', 'anotherTag']);
    var temp = task.clone();
    expect(temp.id).to.not.equal(task.id);
    expect(temp.hasTag('oldTag')).to.equal(false);
    expect(temp.hasTag('anotherTag')).to.equal(false);
    expect(temp.tags.length).to.equal(0);
  });

  it('chaining works', function() {
    task.addTag('bloopTag');
    var temp = task.clone().removeTag('bloopTag').addTag('whateverTask');
    expect(temp.id).to.not.equal(task.id);
    expect(temp.hasTag('bloopTag')).to.equal(false);
    expect(temp.hasTag('whateverTask')).to.equal(true);
    expect(task.hasTag('bloopTag')).to.equal(true);
    expect(task.hasTag('whateverTask')).to.equal(false);
  });




});