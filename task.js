/*
 * task.js
 *
 * Contains implementation for a "task" "class"
 */

var Task, proto, generateID;

// Helper method. You should not need to change it.
// Use it in makeTaskFromString
function processString(s) {
   //"use strict";
   var tags, title;

   tags = [];
   title = s.replace(/\s*#([a-zA-Z]+)/g, function(m, tag) {
      tags.push(tag);

      return '';
   });

   return { title: title, tags: tags };
}

/*
 *       Constructors
 */
// function makeID() {
//    var id;

//    id = 0;

//    function generate() {
//       id += 1;

//       return id;
//    }

//    return {
//       generate: generate
//    };
// }

generateID = (function() {
   var id;

   id = 0;

   function generateID() {
      id += 1;

      return id;
   }

   return generateID;
}());

function makeNewTask() {
   var task;

   task = Object.create(proto);

   Object.defineProperty(task, 'id', {
      configurable: false,
      writable: false,
      enumerable: true,
      value: generateID()
   });

   // Object.defineProperty(task, 'title', {value: ''});

   // Object.defineProperty(task, 'completedTime', null);

   Object.defineProperty(task, 'tags', {
      configurable: false,
      writable: false,
      enumerable: false,
      value: []
   });

   task['title'] = '';
   task['completedTime'] = null;

   Object.preventExtensions(task);

   return task;
}

function makeTaskFromObject(o) {
   var task;

   task = Task.new();

   task.setTitle(o.title);
   task.addTags(o.tags);

   return task;
}

function makeTaskFromString(str) {
   return makeTaskFromObject(processString(str));
}

/*
 *       Prototype / Instance methods
 */

proto = {
//Add instance methods here
   setTitle : function setTitle(s) {
      this.title = s;
   },
   isCompleted : function isCompleted() {
      if (this.completedTime === null) {
         return false;
      }

      return true;
   },
   toggleCompleted : function toggleCompleted() {
      if (this.completedTime === null) {
         this.completedTime = Date.now();
      } else {
         this.completedTime = null;
      }

      return this;

   },
   hasTag : function hasTag(tag) {
      if (this.tags.indexOf(tag) > -1) {
         return true;
      }

      return false;

   },
   addTag : function addTag(tag) {
      if (!this.hasTag(tag)) {
         this.tags.push(tag);
      }

      return this;
   },
   removeTag: function removeTag(tag) {
      if (this.hasTag(tag)) {
         this.tags.splice(this.tags.indexOf(tag), 1);
      }

      return this;
   },
   toggleTag : function toggleTag(tag) {
      if (this.hasTag(tag)) {
         this.removeTag(tag);
      } else {
         this.addTag(tag);
      }

      return this;
   },
   addTags : function addTags(arr) {
      var that;

      that = this;

      arr.forEach(function(t) {
         that.addTag(t);
      });

      return this;
   },
   removeTags : function removeTags(arr) {
      var that;

      that = this;

      arr.forEach(function(t) {
         that.removeTag(t);
      });

      return this;
   },
   toggleTags : function toggleTags(arr) {
      var that;

      that = this;

      arr.forEach(function(t) {
         that.toggleTag(t);
      });

      return this;
   },
   clone : function clone() {
      var newTask;

      newTask = makeNewTask();
      newTask.completedTime = this.completedTime;

      newTask.setTitle(this.title);
      newTask.addTags(this.tags);

      return newTask;
   }
};


// DO NOT MODIFY ANYTHING BELOW THIS LINE
Task = {
   new: makeNewTask,
   fromObject: makeTaskFromObject,
   fromString: makeTaskFromString
};

Object.defineProperty(Task, 'prototype', {
   value: proto,
   writable: false
});

module.exports = Task;
